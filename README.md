# Cali Weather Predictor - CalHacks 2017 Entry

Our app gives the current weather metrics of a given city (or the user's location),
as well as the predicted metrics for the same location in 2100. It uses data from
Lawrence Lab to compute this forecast, and the Weather Underground API to get
current temperatures. For comparison, weather data from previous decades is accessible.
Amazon Web Services was used to host data and integrate it with our app.

## Authors

* Anindit Gopalakrishnan
* Nilay Khatore
* Ankur Mahesh
* Omeed Tavasoli
