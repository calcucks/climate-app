//
//  SearchCityViewController.swift
//  FutureClimateApp
//
//  Created by Ankur Mahesh on 10/7/17.
//  Copyright © 2017 Ankur Mahesh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

class SearchCityViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var outputTempLabel: UILabel!
    @IBOutlet weak var inputCity: UITextField!
    
    var locValue:CLLocationCoordinate2D? = nil
    
    let locationManager = CLLocationManager()

    
    @IBAction func findTempPressed(_ sender: Any) {
        var inputCityString = inputCity.text!
        inputCityString = inputCityString.replacingOccurrences(of: " ", with: "_")
        //Make a call to WUnderground API, and use swiftyJSON to parse the output
        Alamofire.request("http://api.wunderground.com/api/4db10ac251d88aac/conditions/q/CA/\(inputCityString).json").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                if let resData = swiftyJsonVar["current_observation"]["temperature_string"].string {
                    //self.arrRes = resData as! [[String:AnyObject]]
                    print(resData)
                    self.outputTempLabel.text = resData
                }
            }
        }

    }

    @IBAction func findTempCurrLocPressed(_ sender: Any) {
        let currLat = locValue!.latitude
        let currLon = locValue!.longitude
        Alamofire.request(String(format: "http://api.wunderground.com/api/4db10ac251d88aac/conditions/q/%f,%f.json",currLat,currLon)).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                if let resData = swiftyJsonVar["current_observation"]["temperature_string"].string {
                    //self.arrRes = resData as! [[String:AnyObject]]
                    print(resData)
                    self.outputTempLabel.text = resData
                }
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestAlwaysAuthorization()
        
        // Set up Corelocation Manager
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.hideKeyboard()
        // Do any additional setup after loading the view.
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locValue = manager.location!.coordinate
        print("locations = \(locValue?.latitude) \(locValue?.longitude)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
        
        
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
