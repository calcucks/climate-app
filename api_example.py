import urllib2
import json

def weather_output(city):
    """City is a string. For cities with multiple words, remember underscore."""
    city_data = urllib2.urlopen('http://api.wunderground.com/api/4db10ac251d88aac/conditions/q/CA/' + city + '.json')
    json_str = city_data.read()
    parsed = json.loads(json_str)
    location = str(parsed['current_observation']['display_location']['full'])
    city_temp = str(parsed['current_observation']['temp_f'])
    return "Current temperature in " + location + ", is " + city_temp + " degrees F."

def monthly_history_output():
    "Monthly historical averages."
    hist_data = urllib2.urlopen('http://api.wunderground.com/api/4db10ac251d88aac/conditions/q/CA/34.2164,-119.0376.json')
    json_str = hist_data.read()
    parsed = json.loads(json_str)
    title = str(parsed['trip']['title'])
    print(title)
    start_pretty = str(parsed['trip']['period_of_record']['date_start']['date']['pretty'])
    end_pretty = str(parsed['trip']['period_of_record']['date_end']['date']['pretty'])
    print(start_pretty + " to " + end_pretty)
    high_avg = int(str(parsed['trip']['temp_high']['avg']['F']))
    low_avg = int(str(parsed['trip']['temp_low']['avg']['F']))
    avg = (high_avg + low_avg) / 2
    print(avg)

def monthly_wground_output(lat, lon, yyyymmdd):
    "Monthly historical averages."
    nearby_cities = urllib2.urlopen('http://api.wunderground.com/api/4db10ac251d88aac/geolookup/q/' + str(lat) + ',' + str(lon) + '.json')
    json_str = nearby_cities.read()
    parsed = json.loads(json_str)
    city = str(parsed['location']['city']).replace(' ', '_')
    weather = urllib2.urlopen('http://api.wunderground.com/api/4db10ac251d88aac/history_' + str(yyyymmdd) + '/q/CA/' + city + '.json')
    json_weather = weather.read()
    parsed_weather = json.loads(json_weather)
    mean_temp = int(parsed_weather['history']['dailysummary'][0]['meantempi'])
    avg_hum = (int(parsed_weather['history']['dailysummary'][0]['minhumidity']) + int(parsed_weather['history']['dailysummary'][0]['maxhumidity'])) / 2
    return "Temperature on this day was " + str(mean_temp) + ", with humidity " + str(avg_hum) + "."""

def monthly_mesowest_output(lat, lon, yyyymm):
    "Gets weather for a month in history using lat and lon and mm. NEGATE LONGITUDE IF IN DEGREES WEST."
    stations = urllib2.urlopen('http://api.mesowest.net/v2/stations/metadata?state=ca&radius=' + str(lat) + ',' + str(lon)
                + ',50&limit=10&token=c12f8e67b35746ada992c62ecb7c826e')
    json_stations = stations.read()
    parsed_stations = json.loads(json_stations)
    print(parsed_stations)
    valid_stations = []
    min_distance = 50
    station_id = ''
    for station in parsed_stations['STATION']:
        if station['PERIOD_OF_RECORD']['start'] != None and int(str(station['PERIOD_OF_RECORD']['start'][:4]) + str(station['PERIOD_OF_RECORD']['start'][5:7])) == 197001:
            if station['PERIOD_OF_RECORD']['end'] != None and int(str(station['PERIOD_OF_RECORD']['end'][:4]) + str(station['PERIOD_OF_RECORD']['end'][5:7])) >= yyyymm:
                valid_stations.append(station)
    print(valid_stations)
    for station in valid_stations:
        if int(station['DISTANCE']) < min_distance:
            min_distance = int(station['DISTANCE'])
            station_id = str(station['STID'])
    print(station_id)
    month = urllib2.urlopen('http://api.mesowest.net/v2/stations/timeseries?token=c12f8e67b35746ada992c62ecb7c826e'
             + '&stid=' + station_id + '&start=' + str(yyyymm) + '010000&end=' + str(yyyymm) + '280000&units=english')
    json_month = month.read()
    parsed_month = json.loads(json_month)
    print(parsed_month)
    """month = urllib2.urlopen('http://api.mesowest.net/v2/stations/climatology?stid=' + station_id
             + '&startclim=' + str(yyyymm) + '010000&endclim=' + str(yyyymm) + '280000&token=c12f8e67b35746ada992c62ecb7c826e')
    json_month = month.read()
    parsed_month = json.loads(json_month)
    print(parsed_month)"""
